//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef __smtk_mesh_xms_GenerateMesh_h
#define __smtk_mesh_xms_GenerateMesh_h

#include "smtk/mesh/xms/Exports.h"

#include "smtk/operation/XMLOperation.h"

namespace smtk
{
namespace mesh
{
namespace xms
{
class SMTKXMSMESH_EXPORT GenerateMesh : public smtk::operation::XMLOperation
{
public:
  smtkTypeMacro(smtk::mesh::xms::GenerateMesh);
  smtkCreateMacro(GenerateMesh);
  smtkSharedFromThisMacro(smtk::operation::Operation);

  virtual bool ableToOperate() override;

protected:
  virtual Result operateInternal() override;
  virtual const char* xmlDescription() const override;
};
}
}
}

#endif
