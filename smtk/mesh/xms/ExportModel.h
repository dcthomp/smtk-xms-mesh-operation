//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef __smtk_mesh_xms_ExportModel_h
#define __smtk_mesh_xms_ExportModel_h

#include "smtk/mesh/xms/Exports.h"

#include "smtk/mesh/xms/EdgeMap.h"
#include "smtk/mesh/xms/ElementSizing.h"
#include "smtk/mesh/xms/RefinementSources.h"

#include "smtk/model/Model.h"

#include <xmscore/misc/StringUtil.h>
#include <xmsmesh/meshing/MeMultiPolyMesherIo.h>

namespace smtk
{
namespace mesh
{
namespace xms
{

/// Export an smtk::model::Model into the components that comprise a XMS
/// polygonal representation of a 2-d model.
class SMTKXMSMESH_EXPORT ExportModel
{
public:
  ExportModel();
  ExportModel(ElementSizing&&, RefinementSources&&);

  ~ExportModel();

  EdgeMap& edgeMap();
  ElementSizing& elementSizing();
  RefinementSources& refinementSources();

  ::xms::VecPt3d make_edge(const smtk::model::Edge& edge);
  ::xms::VecPt3d make_loop(const smtk::model::Loop& loop, bool reverse_orientation = false);
  ::xms::MeMultiPolyMesherIo make_face(const smtk::model::Face& face,
                                     bool reverse_orientation = false);

private:
  ExportModel(const ExportModel&) = delete;
  ExportModel& operator=(const ExportModel&) = delete;

  void add_refinement_lines(const smtk::model::Face& face, ::xms::VecPt3d2d& insidePolys);
  void add_refinement_points(const smtk::model::Face& face, ::xms::MeMultiPolyMesherIo& input);

  class Internal;
  Internal* m_internal;
};

}
}
}

#endif
