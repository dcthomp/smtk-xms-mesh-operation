set(library_name "_smtkPybindXMSMesh")
set(module_path "smtkxmsmesh")
set(build_path "${PROJECT_BINARY_DIR}/${module_path}")
set(install_path "${PYTHON_MODULEDIR}/${module_path}")

pybind11_add_module(${library_name} PybindXMSMesh.cxx)
target_include_directories(${library_name} PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
  )
target_link_libraries(${library_name} LINK_PUBLIC
  smtkXMSMesh
  smtkCore
  )
set_target_properties(${library_name}
  PROPERTIES
    COMPILE_FLAGS ${PYBIND11_FLAGS}
    LIBRARY_OUTPUT_DIRECTORY "${build_path}"
    LIBRARY_OUTPUT_DIRECTORY_DEBUG "${build_path}"
    LIBRARY_OUTPUT_DIRECTORY_MINSIZEREL "${build_path}"
    LIBRARY_OUTPUT_DIRECTORY_RELEASE "${build_path}"
    LIBRARY_OUTPUT_DIRECTORY_RELWITHDEBINFO "${build_path}"
  )

# Install library
install(TARGETS ${library_name} DESTINATION "${install_path}")

# Create and install module __init__.py
configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/__init__.py"
  "${build_path}/__init__.py" @ONLY
  )

install(
  FILES "${build_path}/__init__.py"
  DESTINATION "${install_path}"
  )
