//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/mesh/xms/ExportModel.h"

#include "smtk/model/Edge.h"
#include "smtk/model/EdgeUse.h"
#include "smtk/model/Face.h"
#include "smtk/model/FaceUse.h"
#include "smtk/model/Loop.h"
#include "smtk/model/Tessellation.h"

#include <xmsmesh/meshing/MeMultiPolyMesher.h>
#include <xmsmesh/meshing/MePolyRedistributePts.h>

namespace smtk
{
namespace mesh
{
namespace xms
{

class ExportModel::Internal
{
public:
  Internal()
    : m_elementSizing()
    , m_refinementSources()
    , m_edgeMap()
    {

    }

  Internal(ElementSizing&& elementSizing, RefinementSources&& refinementSources)
    : m_elementSizing(elementSizing)
    , m_refinementSources(refinementSources)
    , m_edgeMap()
    {

    }

  ElementSizing m_elementSizing;
  RefinementSources m_refinementSources;
  EdgeMap m_edgeMap;
};

ExportModel::ExportModel()
{
  m_internal = new Internal();
}

ExportModel::ExportModel(ElementSizing&& elementSizing, RefinementSources&& refinementSources)
{
  m_internal = new Internal(std::forward<ElementSizing>(elementSizing),
                            std::forward<RefinementSources>(refinementSources));
}

ExportModel::~ExportModel()
{
  delete m_internal;
}

ElementSizing& ExportModel::elementSizing()
{
  return m_internal->m_elementSizing;
}

RefinementSources& ExportModel::refinementSources()
{
  return m_internal->m_refinementSources;
}

EdgeMap& ExportModel::edgeMap()
{
  return m_internal->m_edgeMap;
}

::xms::VecPt3d ExportModel::make_edge(const smtk::model::Edge& edge)
{
  ::xms::VecPt3d output;

  const smtk::model::Tessellation* tess = edge.hasTessellation();
  if(tess == nullptr)
    {
    return output;
    }

  typedef smtk::model::Tessellation Tess;

  //The first thing we do is build vectors of the offsets and length of each
  //polyline in the connectivity, this will allow us to make it easier to
  //determine when we are on the last polyline of the tessellation.
  //But will worsen performance as we have increased memmory overhead,
  //and also had to iterate the data twice.

  const std::vector<Tess::size_type>& conn = tess->conn();
  //the array is [type, len, elements..., type, len, elemenents...], so
  //a line will take up 4 entries in the conn array at minimum
  std::vector< Tess::size_type > offsets; offsets.reserve(conn.size()/4);

  std::size_t sum = 1; //account for the final edge point
  for(std::size_t index = 0; index!=conn.size();)
    {
    const Tess::size_type cell_shape = conn[index] & smtk::model::TESS_CELLTYPE_MASK;
    const int numVerts = conn[index+1];
    if(cell_shape == smtk::model::TESS_POLYLINE)
      {
      offsets.push_back(index+1);
      sum += (numVerts-1);
      }
    //increment by the length of this cell info ( type, len, elements )
    index += (numVerts+2);
    }

  //First reset where we add items into the vector, and that make sure
  //our capactity is sufficient. This is done so that output item can
  //be reused and not re-allocate while we add items
  output.resize(0);
  output.reserve(sum);

  const std::vector<double>& coords = tess->coords();
  for(auto offset_iter=offsets.cbegin(); offset_iter!=offsets.cend(); ++offset_iter)
    {
    auto conn_iter = conn.cbegin() + (*offset_iter);

    //Note:
    //We don't want the last point on the segment as that will duplicated
    //as the first point of the next segment. Unless we are on the last
    //segment and than we need the final point so that the output reaches
    //the full length
    Tess::size_type numVerts = *conn_iter - 1;
    if(offset_iter == offsets.end()-1)
      {
      numVerts +=1;
      }

    ++conn_iter; //increment to pass the numVerts entry
    for(std::size_t j=0; j!=numVerts; ++j, ++conn_iter)
      {
      const Tess::size_type coord_index = *conn_iter * 3;

      ::xms::Pt3d xyz(coords[coord_index],
                    coords[coord_index+1],
                    coords[coord_index+2]);
      output.push_back(xyz);
      }
    }

  const double size = m_internal->m_elementSizing.sizing(edge);
  auto refiner( ::xms::MePolyRedistributePts::New() );
  refiner->SetConstantSizeFunc( size );

  if( size > 0 && output.size() >= 2 )
  {
    if(m_internal->m_refinementSources.isHardEdge(edge))
    {
      //We have a hard edge, so every point in the tessellation is required to
      //exist. We therefore discretize each segment of the hard edge.
      ::xms::VecPt3d in, out, tmp;
      for(std::size_t i = 1; i < output.size(); i++)
      {
        in = ::xms::VecPt3d{{output[i - 1], output[i]}};
        out = refiner->Redistribute(in);

        //Our refiner has a tricky habit of returning an empty vector when the
        //input points are too close together for the selected sizing. To
        //fix this, we add the input points when the refiner fails.
        ::xms::VecPt3d* v = out.empty() ? &in : &out;

        tmp.insert(tmp.end(), (i == 1 ? v->begin() : std::next(v->begin())), v->end());
      }
      output = std::move(tmp);
    }
    else
    {
      //We interpolate at this point instead of with the final loop, so that
      //we don't remove model vertices by mistake, since the poly redistribute
      //only promises to keep the first and last point intact.
      output = refiner->Redistribute(output);
    }
  }

  m_internal->m_edgeMap.emplace(std::make_pair(edge, output));

  return output;
}

::xms::VecPt3d ExportModel::make_loop(const smtk::model::Loop& loop, bool reverse_orientation)
{
  ::xms::VecPt3d output;

  smtk::model::EdgeUses euses = loop.edgeUses();

  for(auto eu=euses.crbegin(); eu!=euses.crend(); ++eu)
  {
    smtk::model::Edge e = eu->edge();

    auto i = m_internal->m_edgeMap.find(e);

    if (i == m_internal->m_edgeMap.end())
    {
      this->make_edge(eu->edge());
      i = m_internal->m_edgeMap.find(e);
    }

    const ::xms::VecPt3d& edgeMesh = i->second;

    // Windows has issues inserting iterators from an empty vector, so we guard against it here.
    if (edgeMesh.empty())
    {
      continue;
    }

    //Flip the orientation based on the edge use orientation
    int orientation = eu->orientation();
    if( reverse_orientation )
    { //flips the orientation as orientation will also be -1 or +1
      orientation *= -1;
    }

    if(orientation == 1)
    {
      output.insert( output.cend(), edgeMesh.crbegin()+1, edgeMesh.crend() );
    }
    else
    {
      output.insert( output.cend(), edgeMesh.cbegin()+1, edgeMesh.cend() );
    }
  }
  return output;
}

//----------------------------------------------------------------------------
void ExportModel::add_refinement_lines(const smtk::model::Face& face, ::xms::VecPt3d2d& insidePolys)
{

  const std::vector<RefinementLine>& lines = m_internal->m_refinementSources.lines(face);
  if (lines.size() == 0)
  {
    return;
  }

  //construct a refiner to use for lines that have a sizing field
  auto refiner( ::xms::MePolyRedistributePts::New() );

  //add all the lines
  for ( std::size_t i=0; i < lines.size(); ++i)
  {
    const RefinementLine& current_RefinementLine = lines[i];

    ::xms::VecPt3d current_breakLine;
    current_breakLine.reserve(2);
    current_breakLine.push_back( current_RefinementLine.start );
    current_breakLine.push_back( current_RefinementLine.end );

    if(current_RefinementLine.sizing > 0)
      {
      //the line needs to be refined. When this happens we have to go up
      //one side of the line, and back down the other. So after refinement
      // we might have
      // 0 , 1 , 2
      // we need to make it
      // 0, 1, 2, 1
      refiner->SetConstantSizeFunc( current_RefinementLine.sizing );

      current_breakLine = refiner->Redistribute(current_breakLine);

      current_breakLine.insert( current_breakLine.end(),
                                current_breakLine.rbegin()+1,
                                current_breakLine.rend()-1 );
      }

    insidePolys.push_back(current_breakLine);
  }
}

//----------------------------------------------------------------------------
void ExportModel::add_refinement_points(const smtk::model::Face& face,
                                        ::xms::MeMultiPolyMesherIo& input)
{
  const std::vector<RefinementPoint>& points = m_internal->m_refinementSources.points(face);
  for ( auto i = points.cbegin(); i != points.end(); ++i)
  {
    if(i->hardPointState != RefinementPoint::OFF)
    {
      const bool isAHardPoint = (i->hardPointState == RefinementPoint::HARDPOINT);
      ::xms::MeRefinePoint refinePoint(i->xyz, i->sizing, isAHardPoint);
      input.m_refPts.push_back(refinePoint);
    }
  }
}

//----------------------------------------------------------------------------
::xms::MeMultiPolyMesherIo ExportModel::make_face(const smtk::model::Face& face,
                                                bool reverse_orientation)
{
  //
  //IMPORTANT: The loop order between smtk and xms are opposite.
  // SMTK:
  // - exterior loops are ccw
  // - interior loops are cw
  // XMS:
  // - exterior loops are cw
  // - interior loops are ccw
  //
  // This means to do an efficient conversion we walk the SMTK loops backwards
  // and when using the edge use we reverse each orientation.
  //
  //
  // In some cases the SMTK loops are actually in the same order as
  // XMS, this occurs when the underlying SMTK model session is
  // based on some other mesh/models codes. This is the reason why
  // we have the reverse_orientation flag

  ::xms::MePolyInput poly;

  //step 1 get the face use for the face
  smtk::model::FaceUse fu_p = face.positiveUse();

  //check if we have an exterior loop
  smtk::model::Loops exteriorLoop = fu_p.loops();
  if(exteriorLoop.size() == 0)
    {
    //if we don't have loops we are bailing out!
    return ::xms::MeMultiPolyMesherIo();
    }

  //currently we presume we only have a single exterior loop, that sounds
  //like a reasonable assumption to me
  smtk::model::Loops interiorLoops = exteriorLoop[0].containedLoops();

  ::xms::VecPt3d& exteriorPoly = poly.m_outPoly;
  ::xms::VecPt3d2d& interiorPolys = poly.m_insidePolys;

  exteriorPoly = this->make_loop(exteriorLoop[0], reverse_orientation);

  //Convert all of the interior loops into xms inPolys
  interiorPolys.reserve(interiorLoops.size());
  for(std::size_t i=0; i < interiorLoops.size(); ++i)
  {
    ::xms::VecPt3d loop = this->make_loop(interiorLoops[i], reverse_orientation);

    if (!loop.empty())
    {
      interiorPolys.push_back(::xms::VecPt3d());
      interiorPolys.back().swap(loop);
    }
  }

  //Setup any breaklines that the user has specified
  add_refinement_lines(face, interiorPolys);

  ::xms::MeMultiPolyMesherIo polys;
  polys.m_polys.push_back(poly);

  //Setup any refinement points that the user has specified
  add_refinement_points(face, polys);

  //setup a refine point at the centroid of the face which will influence
  //the mesh around it. While this isn't perfect it is better at producing
  //nicer meshes than the old bounding / paving approach
  {
    polys.m_polys.begin()->m_constSizeFunction = m_internal->m_elementSizing.sizing(face);
    polys.m_polys.begin()->m_constSizeBias = m_internal->m_elementSizing.bias(face);
  }

  return polys;
}

}
}
}
