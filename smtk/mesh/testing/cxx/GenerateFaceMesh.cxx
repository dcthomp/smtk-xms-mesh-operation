//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"

#include "smtk/mesh/core/Resource.h"

#include "smtk/mesh/xms/operators/GenerateMesh.h"

#include "smtk/model/Model.h"

#include "smtk/session/discrete/Resource.h"
#include "smtk/session/discrete/Session.h"
#include "smtk/session/discrete/operators/ImportOperation.h"

namespace
{
std::string dataRoot = SMTK_XMS_DATA_DIR;
}

int GenerateFaceMesh(int argc, char** const argv)
{
  (void)argc;
  (void)argv;

  // Create an import operator
  smtk::session::discrete::ImportOperation::Ptr importOp =
    smtk::session::discrete::ImportOperation::create();
  if (!importOp)
  {
    std::cerr << "No import operator\n";
    return 1;
  }

  // Set the file path
  std::string fileName = dataRoot + "/test2D.2dm";
  importOp->parameters()->findFile("filename")->setValue(fileName);

  // Execute the operation
  smtk::operation::Operation::Result importOpResult = importOp->operate();

  // Retrieve the resulting model
  smtk::attribute::ComponentItemPtr componentItem =
    std::dynamic_pointer_cast<smtk::attribute::ComponentItem>(
      importOpResult->findComponent("model"));

  // Access the generated model
  smtk::model::Entity::Ptr model =
    std::dynamic_pointer_cast<smtk::model::Entity>(componentItem->value());

  // Test for success
  if (importOpResult->findInt("outcome")->value() !=
    static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
  {
    std::cerr << "Import operation failed\n";
    return 1;
  }

  smtk::model::Model model2dm = model->referenceAs<smtk::model::Model>();

  if (!model2dm.isValid())
  {
    std::cerr << "Reading 2dm file failed!\n";
    return 1;
  }


  auto generateMeshOp = smtk::mesh::xms::GenerateMesh::create();

  generateMeshOp->parameters()->associate(model);

  auto generateMeshOpResult = generateMeshOp->operate();

  // Test for success
  if (generateMeshOpResult->findInt("outcome")->value() !=
    static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
  {
    std::cerr << "Generate Mesh from Tessellation operation failed\n";
    return 1;
  }

  smtk::mesh::Resource::Ptr meshResource = std::dynamic_pointer_cast<smtk::mesh::Resource>(
    generateMeshOpResult->findResource("resource")->value());

  std::cout<<"meshes: "<<meshResource->meshes().size()<<std::endl;
  std::cout<<"cells: "<<meshResource->cells().size()<<std::endl;
  std::cout<<"points: "<<meshResource->points().size()<<std::endl;
  std::cout<<std::endl;

  std::cout<<"3D cells: "<<meshResource->cells(smtk::mesh::Dims3).size()<<std::endl;
  std::cout<<"2D cells: "<<meshResource->cells(smtk::mesh::Dims2).size()<<std::endl;
  std::cout<<"1D cells: "<<meshResource->cells(smtk::mesh::Dims1).size()<<std::endl;
  std::cout<<"0D cells: "<<meshResource->cells(smtk::mesh::Dims0).size()<<std::endl;

  return 0;
}
