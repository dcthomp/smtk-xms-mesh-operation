//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"

#include "smtk/mesh/core/Resource.h"

#include "smtk/mesh/xms/operators/GenerateMesh.h"

#include "smtk/model/EntityIterator.h"
#include "smtk/model/EntityRef.h"
#include "smtk/model/Model.h"

#include "smtk/session/polygon/Resource.h"
#include "smtk/session/polygon/Session.h"
#include "smtk/session/polygon/operators/LegacyRead.h"

namespace
{
std::string dataRoot = SMTK_XMS_DATA_DIR;
}

int GenerateFaceMeshFromPolygon(int argc, char** const argv)
{
  (void)argc;
  (void)argv;

  // Create a read operator
  smtk::session::polygon::LegacyRead::Ptr readOp = smtk::session::polygon::LegacyRead::create();
  if (!readOp)
  {
    std::cerr << "No read operator\n";
    return 1;
  }

  // Set the file path
  std::string fileName = dataRoot + "/boxWithHole.smtk";
  readOp->parameters()->findFile("filename")->setValue(fileName);

  // Execute the operation
  smtk::operation::Operation::Result readOpResult = readOp->operate();

  // Retrieve the resulting resource
  auto resource = smtk::dynamic_pointer_cast<smtk::session::polygon::Resource>(
    readOpResult->findResource("resource")->value(0));

  // Access the generated model
  smtk::model::Entity::Ptr model =
    (*resource->entitiesMatchingFlagsAs<smtk::model::EntityRefs>(
      smtk::model::MODEL_ENTITY).begin()).entityRecord();

  // Test for success
  if (readOpResult->findInt("outcome")->value() !=
    static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
  {
    std::cerr << "Read operation failed\n";
    return 1;
  }

  auto generateMeshOp = smtk::mesh::xms::GenerateMesh::create();

  generateMeshOp->parameters()->associate(model);

  auto generateMeshOpResult = generateMeshOp->operate();

  // Test for success
  if (generateMeshOpResult->findInt("outcome")->value() !=
    static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
  {
    std::cerr << "Generate Mesh from Tessellation operation failed\n";
    return 1;
  }

  smtk::mesh::Resource::Ptr meshResource = std::dynamic_pointer_cast<smtk::mesh::Resource>(
    generateMeshOpResult->findResource("resource")->value());

  std::cout<<"meshes: "<<meshResource->meshes().size()<<std::endl;
  std::cout<<"cells: "<<meshResource->cells().size()<<std::endl;
  std::cout<<"points: "<<meshResource->points().size()<<std::endl;
  std::cout<<std::endl;

  std::cout<<"3D cells: "<<meshResource->cells(smtk::mesh::Dims3).size()<<std::endl;
  std::cout<<"2D cells: "<<meshResource->cells(smtk::mesh::Dims2).size()<<std::endl;
  std::cout<<"1D cells: "<<meshResource->cells(smtk::mesh::Dims1).size()<<std::endl;
  std::cout<<"0D cells: "<<meshResource->cells(smtk::mesh::Dims0).size()<<std::endl;

  return 0;
}
