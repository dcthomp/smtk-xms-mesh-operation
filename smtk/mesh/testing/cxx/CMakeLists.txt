set(unit_tests
)

set(unit_tests_which_require_data
)

set(external_libs)

if (TARGET smtkDiscreteSession)
  list(APPEND unit_tests_which_require_data
    GenerateFaceMesh.cxx
    GenerateRefinedFaceMesh.cxx
    )
  list(APPEND external_libs
    smtkDiscreteSession
    )
endif()

if (TARGET smtkPolygonSession)
  list(APPEND unit_tests_which_require_data
    GenerateFaceMeshFromPolygon.cxx
    )
  list(APPEND external_libs
    smtkPolygonSession
    )
endif()

smtk_xms_unit_tests(
  LABEL "XMS Mesh"
  SOURCES ${unit_tests}
  SOURCES_REQUIRE_DATA ${unit_tests_which_require_data}
  LIBRARIES smtkCore smtkXMSMesh smtkCoreModelTesting ${external_libs}
)
